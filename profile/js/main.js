(function($) {
    "use strict";

    $(document).ready(function() {

        $('#fullpage').fullpage({
            anchors: ['firstPage', 'secondPage', '3rdPage', '4rdPage'],
            sectionsColor: ['#fff', '#fff', '#fff', '#39295a'],
            navigation: true,
            navigationPosition: 'right',
            navigationTooltips: ['Welcome', 'About me', 'Portfolio', 'Contact me']
        });

        var s = $('#section0 .intro');
        $('#section0').hover(
            function() {
                s.addClass('effect');
            },
            function() {
                s.removeClass('effect');
            }
        );

        $('.box-left').hover(
            function() {
                $('.box-right').addClass('blur')
            },
            function() {
                $('.box-right').removeClass('blur')
            }
        );

        $('.box-right').hover(
            function() {
                $('.box-left').addClass('blur')
            },
            function() {
                $('.box-left').removeClass('blur')
            }
        );


    });

})(jQuery);