// DINAMYC FULL HEIGHT AND WIDTH BACKGROUND IMAGE ON WINDOW
function setDimensions() {
    var windowsHeight = $(window).height();
    $('#slide .item').css('height', windowsHeight + 'px');
}
//when resizing the site, we adjust the heights of the sections
$(window).resize(function() {
    setDimensions();
});
setDimensions();

$('#menu .link').on('click', function(){
    $('a.active').removeClass('active');
    $(this).addClass('active');
});

// CONTENT SLIDE
$(document).ready(function() {
    $('#mobile-1 #slide').owlCarousel({
        items:1,
        loop:false,
        // animateOut: 'fadeInRight',
        animateIn: 'animate__flipInY',
        center:true,
        mouseDrag: false,
        dots: false,
        nav: false,
        URLhashListener:true,
        startPosition: 'URLHash'
    });

    $('#mobile-2 #slide').owlCarousel({
        items:1,
        loop:false,
        // animateOut: 'fadeInRight',
        animateIn: 'animate__flipInX',
        center:true,
        mouseDrag: false,
        dots: false,
        nav: false,
        URLhashListener:true,
        startPosition: 'URLHash'
    });

    $('#desktop-1 #slide').owlCarousel({
        items:1,
        loop:false,
        // animateOut: 'fadeInRight',
        animateIn: 'animate__flipInY',
        center:true,
        mouseDrag: false,
        dots: false,
        nav: false,
        URLhashListener:true,
        startPosition: 'URLHash'
    });

    $('#desktop-2 #slide').owlCarousel({
        items:1,
        loop:false,
        // animateOut: 'fadeInRight',
        animateIn: 'animate__flipInX',
        center:true,
        mouseDrag: false,
        dots: false,
        nav: false,
        URLhashListener:true,
        startPosition: 'URLHash'
    });

    $('.mobile #menu').owlCarousel({
        items:5,
        loop:false,
        margin:0,
        nav:false,
    });
});


// COUNTDOWN
const second = 1000,
      minute = second * 60,
      hour = minute * 60,
      day = hour * 24;

let countDown = new Date('Jul 30, 2020 00:00:00').getTime(),
    x = setInterval(function() {    

      let now = new Date().getTime(),
          distance = countDown - now;

      document.getElementById('days').innerText = Math.floor(distance / (day)),
        document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),
        document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
        document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);

      //do something later when date is reached
      //if (distance < 0) {
      //  clearInterval(x);
      //  'IT'S MY BIRTHDAY!;
      //}

    }, second);


const seconds = 1000,
      minutes = seconds * 60,
      hours = minutes * 60,
      days = hours * 24;

let countDowns = new Date('Jul 30, 2020 00:00:00').getTime(),
    xs = setInterval(function() {    

      let now = new Date().getTime(),
          distance = countDowns - now;

      document.getElementById('days2').innerText = Math.floor(distance / (days)),
        document.getElementById('hours2').innerText = Math.floor((distance % (days)) / (hours)),
        document.getElementById('minutes2').innerText = Math.floor((distance % (hours)) / (minutes)),
        document.getElementById('seconds2').innerText = Math.floor((distance % (minutes)) / seconds);

      //do something later when date is reached
      //if (distance < 0) {
      //  clearInterval(x);
      //  'IT'S MY BIRTHDAY!;
      //}

    }, seconds);

// MFP
$(document).ready(function() {
    $('.zoom-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        closeOnContentClick: false,
        closeBtnInside: false,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        image: {
            verticalFit: true,
            // titleSrc: function(item) {
            //     return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
            // }
        },
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300, // don't foget to change the duration also in CSS
            opener: function(element) {
                return element.find('img');
            }
        }
        
    });
});

// MUSIC PLAY
var myAudio = document.getElementById("audio");
var isPlaying = false;
var button = $('.btn-play');

function togglePlay() {
  if (isPlaying) {
    myAudio.pause();
    button.removeClass('play');
  } else {
    myAudio.play();
    button.addClass('play');
  }
};
myAudio.onplaying = function() {
  isPlaying = true;
};
myAudio.onpause = function() {
  isPlaying = false;
};

var myAudios = document.getElementById("audios");
var isPlayings = false;
var buttons = $('.btn-play');

function togglePlays() {
  if (isPlayings) {
    myAudios.pause();
    buttons.removeClass('play');
  } else {
    myAudios.play();
    buttons.addClass('play');
  }
};
myAudios.onplaying = function() {
  isPlayings = true;
};
myAudios.onpause = function() {
  isPlayings = false;
};

// MORE BUTTON
const menuMore = $('.more-menu'),
      btnMore = $('.btn-more');

btnMore.click(function (e) {
    menuMore.toggleClass('open');
    e.stopPropagation()
});

$(document).click(function (e) {
    if (! $(e.target).hasClass('more-menu')) menuMore.removeClass('open');
});
